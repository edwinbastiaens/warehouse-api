<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(
        UserRepository $userRepository,
        UserService $userService
    ) {
        $this->userRepository = $userRepository;
        $this->userService = $userService;
    }

    /**
     * Only added this method, to allow for manual testing & code review.
     *
     * @Route("/api/users", name="api_create_admin_user", methods="POST")
     */
    public function createAdminUser(Request $request): Response
    {
        $user = new User();

        if (!$request->request->has('email') || !$request->request->has('password')) {
            return $this->json(['error'=>'missing data'], Response::HTTP_BAD_REQUEST);
        }

        $user->setEmail($request->request->get('email'));
        $user->setRoles(['ROLE_ADMIN']);

        $user->setPassword($this->userService->encodePassword($user, $request->request->get('password')));

        //TODO: catch when user already exists, and return error

        $this->userRepository->persist($user);

        return $this->json(
            ['email' => $user->getEmail()],
            Response::HTTP_CREATED
        );
    }
}
