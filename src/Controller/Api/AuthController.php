<?php

namespace App\Controller\Api;

use App\Repository\UserRepository;
use App\Service\UserService;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{
    /** @var UserRepository */
    private $userRepository;

    /** @var UserService */
    private $userService;

    /** @var JWTEncoderInterface */
    private $jwtEncoder;

    public function __construct(
        UserRepository $userRepository,
        UserService $userService,
        JWTEncoderInterface $jwtEncoder
)
    {
        $this->userRepository = $userRepository;
        $this->userService = $userService;
        $this->jwtEncoder = $jwtEncoder;
    }

    /**
     * @Route("/api/tokens", name="api_create_token", methods="POST")
     */
    public function createToken(Request $request): Response
    {
        if (!$request->request->has('email') || !$request->request->has('password')) {
            return $this->json(['error'=>'missing data'], Response::HTTP_BAD_REQUEST);
        }

        $email = $request->request->get('email');
        $password = $request->request->get('password');
        $user = $this->userRepository->findOneByEmail($email);

        if (!$user || !$this->userService->passwordValid($user, $password)) {
            return $this->json(
                ['error' => 'not authorized'],
                Response::HTTP_UNAUTHORIZED
            );
        }

        $token = $this->jwtEncoder->encode(['uid' => $user->getId(), 'email' => $user->getEmail()]);

        return $this->json(
            ['token' => $token],
            Response::HTTP_CREATED
        );
    }
}
