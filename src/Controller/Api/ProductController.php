<?php

namespace App\Controller\Api;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ProductController constructor.
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @Route("/api/admin/products", name="api_create_product", methods="POST")
     */
    public function createProduct(Request $request): Response
    {
        $product = new Product();

        if (!$request->request->has('name')) {
            return $this->json(['error'=>'missing data'], Response::HTTP_BAD_REQUEST);
        }

        $product->setName($request->request->get('name'));

        if ($request->request->has('price')) {
            $price = (int)$request->request->get('price');
            $product->setPrice($price);
        }

        $this->productRepository->persist($product);

        return $this->json(
            ['code' => $product->getCode()],
            Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/api/products", name="api_get_products", methods="GET")
     */
    public function getProducts(): Response
    {
        $products = $this->productRepository->findAll();

        return $this->json($products, Response::HTTP_OK);
    }

    /**
     * @Route("/api/products/{code}", name="api_get_product", methods="GET")
     */
    public function getProduct(string $code): Response
    {
        $product = $this->productRepository->findOneByCode($code);

        if (!$product) {
            return $this->json(['error' => 'data not found'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json($product, Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/products/{code}", name="api_delete_product", methods="DELETE")
     */
    public function deleteProduct(string $code): Response
    {
        $product = $this->productRepository->findOneByCode($code);

        if ($product) {
            $this->productRepository->remove($product);
        }

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/api/admin/products/increasestock", name="api_product_increase_stock", methods="PATCH")
     */
    public function increaseProductStock(Request $request): Response
    {
        $code = $request->request->get('code');
        $amount = (int)$request->request->get('amount');
        if (!$code || !$amount) {
            return $this->json('data incomplete', Response::HTTP_BAD_REQUEST);
        }

        $product = $this->productRepository->findOneByCode($code);
        if (!$product) {
            return $this->json('data incorrect', Response::HTTP_BAD_REQUEST);
        }

        $product->increaseStock($amount);
        $this->productRepository->persist($product);

        return $this->json(['code' => $code, 'stock' => $product->getStock()], Response::HTTP_OK);
    }
}
