<?php

namespace App\Controller\Api;

use App\Entity\Purchase;
use App\Service\PurchaseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PurchaseController extends AbstractController
{
    /** @var PurchaseService */
    private $purchaseService;

    public function __construct(PurchaseService $purchaseService)
    {
        $this->purchaseService = $purchaseService;
    }

    /**
     * @Route("/api/purchases", name="api_create_purchase", methods="POST")
     */
    public function createPurchase(Request $request)
    {
        if (!$request->request->has('email') ||!$request->request->has('codes') || !$request->request->has('counts')) {
            return $this->json(['error'=>'missing data'], Response::HTTP_BAD_REQUEST);
        }

        $email = $request->request->get('email');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->json(['error'=>'invalid email'], Response::HTTP_BAD_REQUEST);
        }

        $codes = explode(',', $request->request->get('codes'));
        $counts = explode(',', $request->request->get('counts'));

        if (count($codes) !== count($counts)) {
            return $this->json(
                ['error'=>'number of codes and counts are not equal'],
                Response::HTTP_BAD_REQUEST
            );
        }

        if (!$this->onlyNumbers($counts)) {
            return $this->json(
                ['error'=>'counts are not numeric'],
                Response::HTTP_BAD_REQUEST
            );
        }

        $purchase = $this->purchaseService->makePurchase($email, $codes, $counts);

        if ($purchase && Purchase::COMPLETE_STATUS === $purchase->getStatus()) {
            return $this->json(['purchaseid' => $purchase->getId()], Response::HTTP_CREATED);
        }

        return $this->json([],Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @Route("/api/purchases/{id}", name="api_get_purchase", methods="GET")
     */
    public function getPurchase(int $id)
    {
        $purchaseData = $this->purchaseService->getPurchaseData($id);

        if (!$purchaseData) {
            return $this->json(['error' => 'data not found'], Response::HTTP_BAD_REQUEST);
        }

        return $this->json($purchaseData, Response::HTTP_OK);
    }

    private function onlyNumbers($array): bool
    {
        foreach($array as $value) {
            if (!is_numeric($value)) {
                return false;
            }
        }

        return true;
    }
}
