<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function bulkPersist($products): bool
    {
        try {
            foreach($products as $product) {
                $this->_em->persist($product);
            }
            $this->_em->flush();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function persist(Product $product): bool
    {
        try {
            $this->_em->persist($product);
            $this->_em->flush();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function remove(Product $product): bool
    {
        try {
            $this->_em->remove($product);
            $this->_em->flush();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

     /**
      * @return Product Returns an array of Product objects
      */
    public function findOneByCode($code): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.code = :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function purchaseProducts(array $productCodes, array $productCounts): bool
    {
        $this->_em->getConnection()->beginTransaction();
        try {
            foreach ($productCodes as $index => $productCode) {
                $products[] = $product = $this->findOneByCode($productCode);

                if (!$product) {
                    throw new \Exception('code not found');
                }

                if ($productCounts[$index] > $product->getStock()) {
                    throw new \Exception('not available in stock');
                }
            }

            foreach ($products as $index => $product) {
                $products[$index]->decreaseStock($productCounts[$index]);
            }

            $this->bulkPersist($products);
            $this->_em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->_em->getConnection()->rollBack();

            return false;
        }

        return true;
    }

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
