<?php

namespace App\Service;

use App\Entity\Purchase;
use App\Entity\PurchaseItem;
use App\Repository\ProductRepository;
use App\Repository\PurchaseItemRepository;

class PurchaseItemService
{
    /** @var PurchaseItemRepository */
    private $purchaseItemRepository;

    /** @var ProductRepository */
    private $productRepository;

    public function __construct(
        PurchaseItemRepository $purchaseItemRepository,
        ProductRepository $productRepository
    ) {
        $this->purchaseItemRepository = $purchaseItemRepository;
        $this->productRepository = $productRepository;
    }

    public function makePurchaseItems(
        Purchase $purchase,
        array $productCodes,
        array $productCounts
    ): void {
        $purchaseItems = [];
        foreach($productCodes as $index => $productCode) {
            $product = $this->productRepository->findOneByCode($productCode);
            $count = $productCounts[$index];

            $purchaseItem = new PurchaseItem();
            $purchaseItem->setProduct($product);
            $purchaseItem->setPurchase($purchase);
            $purchaseItem->setPrice($count * $product->getPrice());
            $purchaseItem->setCount($count);

            $purchaseItems[] = $purchaseItem;
        }

        $this->purchaseItemRepository->bulkPersist($purchaseItems);
    }
}
