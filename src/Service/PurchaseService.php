<?php

namespace App\Service;

use App\Entity\Purchase;
use App\Repository\ProductRepository;
use App\Repository\PurchaseRepository;

class PurchaseService
{
    /** @var PurchaseRepository */
    private $purchaseRepository;

    /** @var ProductRepository */
    private $productRepository;

    /** @var PurchaseItemService */
    private $purchaseItemService;


    public function __construct(
        PurchaseRepository $purchaseRepository,
        ProductRepository $productRepository,
        PurchaseItemService $purchaseItemService
    ) {
        $this->purchaseRepository = $purchaseRepository;
        $this->productRepository = $productRepository;
        $this->purchaseItemService = $purchaseItemService;
    }

    public function makePurchase(string $email, array $productCodes, array $productCounts): ?Purchase
    {
        $purchase = new Purchase();
        $purchase->setEmail($email);
        $purchase->setCodes(implode(',',$productCodes));
        $purchase->setCounts(implode(',',$productCounts));
        $this->purchaseRepository->persist($purchase);

        $purchaseSuccess = $this->productRepository->purchaseProducts($productCodes, $productCounts);

        $purchase->setStatus($purchaseSuccess? Purchase::COMPLETE_STATUS : Purchase::REFUSED_STATUS);
        $this->purchaseRepository->persist($purchase);

        if ($purchaseSuccess) {
            //TODO: use eventhandling or bus command for this
            $this->purchaseItemService->makePurchaseItems($purchase, $productCodes, $productCounts);
        }

        return $purchase;
    }

    public function getPurchaseData($purchaseId): array
    {
        $purchase = $this->purchaseRepository->findOneById($purchaseId);

        if (!$purchase) {
            return [];
        }

        return [
            'purchase' => $purchase,
        ];
    }
}
