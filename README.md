### To run this project locally project perform following steps:

1. %> symfony composer update

2. %> docker-compose up -d

3. update the database port in the .env file to the port that was chosen by docker:
 
       %> docker ps 

      Under PORTS you will see eg : 33060/tcp, 0.0.0.0:**32779**->3306/tcp
      => **32779** is the port chosen by docker for MySQL
      => Hence update the DATABASE_URL in the .env file with this port.

4. perform the migrations
    
       %> php bin/console doctrine:migrations:migrate

### API overview

1. create user admin

        POST
        url: api/users
        Body parameters: email + password

2. create user token

        POST
        url: api/tokens
        Body parameters: email + password
        
        Response:  token
        
3. create product

        POST
        url: api/admin/products
        Body parameters : name + price
        Header parameters: 
            key: Authorization
            value: Bearer -retrieved-token-
        
        Response: code  (this is the productcode)

4. increase a products stock

        PATCH
        url: api/admin/products/increasestock
        Body parameters : code + amount
        Header parameters: 
            key: Authorization
            value: Bearer -retrieved-token-
        
        Response: code + stock
        
5. get all products

        GET
        url: api/products
        Body parameters : code + amount
        
        Response: [ products ]
        
6. get a single product

        GET
        url: api/products/{code}
        Body parameters : code + amount
        
        Response: product
        
7. make a purchase

        POST
        url: api/purchases
        Body parameters : codes + counts + email
            > codes to be comma-separated list of valid productcodes
            > counts to be a comma-separated list of positive numbers 
        
        Response: purchase id (in case unsuccessfull : [] )

8. get a purchase

        GET
        url: api/purchases/{purchaseid}
        
        Response: purchase