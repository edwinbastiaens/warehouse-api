<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserServiceTest  extends WebTestCase
{
    /** @var UserService */
    private $userService;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    private $userPasswordEncoderInterfaceProphecy;

    public function setUp()
    {
        $this->userPasswordEncoderInterfaceProphecy = $this->prophesize(UserPasswordEncoderInterface::class);
        $this->userService = new UserService($this->userPasswordEncoderInterfaceProphecy->reveal());
    }

    public function testItEncodesAPassword()
    {
        $user = new User();
        $user->setEmail('test@test.com');

        $this->userPasswordEncoderInterfaceProphecy
            ->encodePassword($user,'abc123')
            ->shouldBeCalledTimes(2)
            ->willReturn('some-value');

        $a = $this->userService->encodePassword($user, 'abc123');
        $b = $this->userService->encodePassword($user, 'abc123');

        $this->assertSame($a,$b);
    }
}
