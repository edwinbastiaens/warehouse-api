<?php

namespace App\Tests\Service;

use App\Entity\Purchase;
use App\Repository\ProductRepository;
use App\Repository\PurchaseRepository;
use App\Service\PurchaseItemService;
use App\Service\PurchaseService;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PurchaseServiceTest extends WebTestCase
{
    /** @var PurchaseService */
    private $purchaseService;

    /** @var ObjectProphecy */
    private $purchaseRepositoryProphecy;

    /** @var ObjectProphecy */
    private $productRepositoryProphecy;

    /** @var ObjectProphecy */
    private $purchaseItemServiceProphecy;

    public function setUp(): void
    {
        $this->productRepositoryProphecy = $this->prophesize(ProductRepository::class);
        $this->purchaseRepositoryProphecy = $this->prophesize(PurchaseRepository::class);
        $this->purchaseItemServiceProphecy = $this->prophesize(PurchaseItemService::class);

        $this->purchaseService = new PurchaseService(
            $this->purchaseRepositoryProphecy->reveal(),
            $this->productRepositoryProphecy->reveal(),
            $this->purchaseItemServiceProphecy->reveal(),
        );
    }

    public function testMakePurchase()
    {
        $email = 'test@test.com';
        $productCodes = ['abc123','def456'];
        $productCounts = [2,1];

        $this->purchaseRepositoryProphecy
            ->persist(Argument::type(Purchase::class))
            ->willReturn(true);

        $this->productRepositoryProphecy
            ->purchaseProducts($productCodes, $productCounts)
            ->shouldBeCalledOnce()
            ->willReturn(true);

        $purchase = $this->purchaseService->makePurchase($email, $productCodes, $productCounts);

        $this->assertSame(Purchase::COMPLETE_STATUS, $purchase->getStatus());
    }

    public function testPurchaseWillBeRefusedIfPurchaseNotSuccesful()
    {
        $email = 'test@test.com';
        $productCodes = ['abc123','def456'];
        $productCounts = [2,1];

        $this->purchaseRepositoryProphecy
            ->persist(Argument::type(Purchase::class))
            ->willReturn(true);

        $this->productRepositoryProphecy
            ->purchaseProducts($productCodes, $productCounts)
            ->shouldBeCalledOnce()
            ->willReturn(false);

        $purchase = $this->purchaseService->makePurchase($email, $productCodes, $productCounts);

        $this->assertSame(Purchase::REFUSED_STATUS, $purchase->getStatus());
    }
}
