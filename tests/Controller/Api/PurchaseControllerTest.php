<?php

namespace App\Tests\Controller\Api;

use App\Controller\Api\PurchaseController;
use App\Entity\Purchase;
use App\Service\PurchaseService;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PurchaseControllerTest extends WebTestCase
{
    /** @var PurchaseController */
    private $purchaseController;

    /** @var ObjectProphecy */
    private $purchaseServiceProphecy;

    public function setUp(): void
    {
        $this->purchaseServiceProphecy = $this->prophesize(PurchaseService::class);
        $this->purchaseController = new PurchaseController($this->purchaseServiceProphecy->reveal());
    }

    public function testPurchaseWontWorkWithoutCodes()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/purchases',
            ['counts' => '','email' => ''],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testPurchaseWontWorkWithoutCounts()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/purchases',
            ['codes' => '', 'email' => ''],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testPurchaseWontWorkWithoutEmail()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/purchases',
            ['codes' => '', 'counts' => ''],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function testItGivesNotAcceptableWHenMakingAPurchaseOfNotExisitingProduct()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/purchases',
            ['codes' => 'unexisting-code,other-unexisting-code', 'counts' => '1,2', 'email' => 'test@test.com'],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NOT_ACCEPTABLE, $response->getStatusCode());
    }

    public function testItGivesCreatedWHenMakingAPurchase()
    {
        $client = static::createClient();

        //make sure we have a product with something in stock
        $data = $this->getAll($client);
        $aProductsCode = $data[0]->code;

        $client->request(
            'PATCH',
            '/api/admin/products/increasestock',
            ['code' => $aProductsCode, 'amount' => 10],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $this->getUseableToken($client),
                'CONTENT_TYPE' => 'application/json'
            ]
        );


        $client->request(
            'POST',
            '/api/purchases',
            ['codes' => $aProductsCode, 'counts' => '2', 'email' => 'test@test.com'],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    private function getUseableToken($client)
    {
        $email = 'jef@mail.com';
        $password = 'abc123';

        $client->request(
            'POST',
            '/api/tokens',
            ['email' => $email, 'password' => $password],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $data = json_decode($response->getContent());
        return $data->token;
    }

    private function getAll(KernelBrowser $client)
    {
        $client->request('GET', '/api/products');
        $response = $client->getResponse();
        $data = json_decode($response->getContent());

        return $data;
    }
}
