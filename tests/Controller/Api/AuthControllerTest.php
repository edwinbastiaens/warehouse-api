<?php

namespace App\Tests\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class AuthControllerTest extends WebTestCase
{
    public function testItProvidesAToken()
    {
        $client = static::createClient();
        $email = 'jef@mail.com';
        $password = 'abc123';

        $client->request(
            'POST',
            '/api/users',
            ['email' => $email, 'password' => $password],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );


        $client->request(
            'POST',
            '/api/tokens',
            ['email' => $email, 'password' => $password],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $data = json_decode($response->getContent());
        $this->assertTrue(property_exists($data,'token'));
    }

    public function testItReturnsErrorIfNoPasswordProvided()
    {
        $client = static::createClient();
        $email = 'jef@mail.com';

        $client->request(
            'POST',
            '/api/tokens',
            ['email' => $email],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }
}
