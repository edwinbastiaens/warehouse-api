<?php

namespace App\Tests\Controller\Api;

use App\Controller\Api\ProductController;
use App\Repository\ProductRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProductControllerTest extends WebTestCase
{
    /** @var ProductController */
    private $productController;

    /** @var \Prophecy\Prophecy\ObjectProphecy */
    private $productRepositoryMock;

    public function setUp(): void
    {
        $this->productRepositoryMock = $this->prophesize(ProductRepository::class);

        $this->productController = new ProductController(
            $this->productRepositoryMock->reveal()
        );
    }

    public function testGetProducts()
    {
        $client = static::createClient();

        $client->request('GET', '/api/products');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetNonExistingProduct()
    {
        $client = static::createClient();
        $client->request('GET', '/api/products/abc123');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertContains('data not found', $response->getContent());
    }

    public function testCreatAndGetProduct()
    {
        $productName = 'boormachine';
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/admin/products',
            ['name' => $productName, 'price' => 99],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $this->getUseableToken($client),
                'CONTENT_TYPE' => 'application/json'
            ]
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $data = json_decode($response->getContent());
        $receivedProductCode = $data->code;

        $client->request('GET', '/api/products/' . $receivedProductCode);
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $this->assertContains($productName, json_encode($response->getContent()));
        $this->assertContains($receivedProductCode, json_encode($response->getContent()));
    }

    private function getAll(KernelBrowser $client)
    {
        $client->request('GET', '/api/products');
        $response = $client->getResponse();
        $data = json_decode($response->getContent());

        return $data;
    }

    public function testGetAllAndIncreaseProductStock()
    {
        $client = static::createClient();
        $data = $this->getAll($client);
        $aProductsCode = $data[0]->code;
        $stock = $data[0]->stock;
        $increaseAmount = 8;

        $client->request(
            'PATCH',
            '/api/admin/products/increasestock',
            ['code' => $aProductsCode, 'amount' => $increaseAmount],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $this->getUseableToken($client),
                'CONTENT_TYPE' => 'application/json'
            ]
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $data = json_decode($response->getContent());
        $this->assertEquals($stock + $increaseAmount, $data->stock);
    }

    public function testIncreaseStockForNonExistingProductFails()
    {
        $client = static::createClient();
        $client->request(
            'PATCH',
            '/api/admin/products/increasestock',
            ['code' => 'abc123', 'amount' => 50],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $this->getUseableToken($client),
                'CONTENT_TYPE' => 'application/json'
            ]
        );
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertContains('data incorrect', $response->getContent());
    }

    public function testDeleteProduct()
    {
        $client = static::createClient();
        $data = $this->getAll($client);
        $elementPosition = count($data) - 1;
        $aProductsCode = $data[$elementPosition]->code;

        $client->request(
            'DELETE',
            '/api/admin/products/' . $aProductsCode,
            [],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $this->getUseableToken($client),
                'CONTENT_TYPE' => 'application/json'
            ]
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());

        $client->request('GET', '/api/products/' . $aProductsCode);
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertContains('data not found', $response->getContent());

    }

    public function testCreatingAProductWillNotWorkWithoutProperAuthToken()
    {
        $productName = 'boormachine';
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/admin/products',
            ['name' => $productName, 'price' => 99],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
    }

    private function getUseableToken($client)
    {
        $email = 'jef@mail.com';
        $password = 'abc123';

        $client->request(
            'POST',
            '/api/tokens',
            ['email' => $email, 'password' => $password],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $data = json_decode($response->getContent());
        return $data->token;
    }

    public function testCreatingAProductNeedsAuthToken()
    {
        $client = static::createClient();
        $email = 'jef@mail.com';
        $password = 'abc123';

        $client->request(
            'POST',
            '/api/tokens',
            ['email' => $email, 'password' => $password],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
        $data = json_decode($response->getContent());
        $token = $data->token;

        $client->request(
            'POST',
            '/api/admin/products',
            ['name' => 'hammer', 'price' => 39],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $token,
                'CONTENT_TYPE' => 'application/json',
            ]
        );

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }
}
