<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201130235045 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchase ADD codes LONGTEXT NOT NULL, ADD counts LONGTEXT NOT NULL, CHANGE status status VARCHAR(16) DEFAULT \'CREATED\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE purchase DROP codes, DROP counts, CHANGE status status VARCHAR(16) CHARACTER SET utf8mb4 DEFAULT \'created\' COLLATE `utf8mb4_unicode_ci`');
    }
}
